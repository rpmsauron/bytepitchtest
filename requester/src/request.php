<?php

require "bootstrap.php";

include_once('ResponseHandler.php');

// Send request to Broker service.
$brokerHost = $_ENV['BROKER_HOST'];

$payload     = json_encode(["message" => "Hi, "]);
$handle      = curl_init();
curl_setopt($handle, CURLOPT_URL, "http://" . $brokerHost . "/api/request");
curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
curl_setopt($handle, CURLOPT_POST, 1);
curl_setopt($handle, CURLOPT_POSTFIELDS, $payload);
$data        = json_decode(curl_exec($handle));
curl_close($handle);

if(!$data) {
    exit("No response from Broker service.\n");
}

// Print out token generated.
echo($data . "\n");

// Pull out a response from Broker service.
$handler = new ResponseHandler();
try {
    $handler->getResponse($data);
} catch(Exception $e) {
    echo $e->getMessage() . "\n";
}
