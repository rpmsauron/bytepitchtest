<?php

/**
 * @author rpsilva
 */

class ResponseHandler 
{
    // Maximum time to try to pull a response from Broker service.
    const MAX      = 1;

    // Interval at which to retry to pull a response on failure.
    const INTERVAL = 0.05;

    private $serviceHost;

    public function  __construct()
    {
        $this->serviceHost = $_ENV['BROKER_HOST'];
    }

    /**
     * Attempts to pull a response from Broker service with a full_message generated 
     * from having called the previous endpoint.
     *
     * @param  string $token The token for which to fetch the full_nessage response 
     *                       from the Broker service.
     * 
     * @throws Exception Thrown when a response with a full_message cannot be 
     *                   retrieved from the Broker service, thus responding with 
     *                   No Response message.
     */
    public function getResponse(string $token)
    {    
        $tryCount  = 0;
        $interval  = self::INTERVAL;
        $active    = true;
        $startTime = microtime(true);
        $nextTime  = $startTime;
        $endTime   = $startTime + self::MAX; 

        while($active) {
            sleep($interval);

            $now = microtime(true);
            if ($now >= $nextTime) {
                $response = $this->pullResponse($token);
                $nextTime = $now + $interval;
                $tryCount++;
            }

            // Redefine condition to keep trying to pull response.
            $active = !isset($response) && microtime(true) < $endTime;
        }

        $lastTime = microtime(true);
        $delta    = $lastTime - $startTime;

        if ($response == null) {
            throw new Exception("No Response");
        } else {
            echo $response . "\nBroker responded after " . $delta
                    . " seconds and on " . $tryCount . " tries.\n";
        }
    }

    /**
     * Pulls a response from the Broker endpoint to retrieve it with a full_message.
     *
     * @param  string $token The token for which to fetch the full_nessage response 
     *                       from the Broker service.
     *
     * @return string The full_message retrieved from the Broker service.
     */
    private function pullResponse(string $token): ?string
    {
        // Send request to Broker service.
        $endpoint = "http://" . $this->serviceHost . "/api/response/" . $token;

        $handle   = curl_init();
        curl_setopt($handle, CURLOPT_URL, $endpoint);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $data     = curl_exec($handle);
        curl_close($handle);

        $dataObject = json_decode($data);

        return $dataObject->full_message;
    }
}
