
# Bytepitch Technical task

## Intro
This project consists on a multi-microservice platform with simple microservices interacting assynchronously with one another, each respectively self-contained environmentally via Docker containers.

It was developed on native PHP version 7.4.5, with Postgres, and integrates with Kafka platform for a Publish/Subscribe architecture for service inter-messaging, with curl requests to simple REST API endpoints.

Developed in March of 2021.

## Context ##
This project is a technical task for technical assessment by Bytepitch for admission purposes.

## Local install ##
#### Requirements: 
To use it locally, you should have Docker / Docker Desktop installed locally to be able to create and run Containers from Docker images. Docker Desktop eases the work but is not mandatory nor even required.
#### Install

1. To install this, get the project first from this repository:
    ```
    git clone git@bitbucket.org:rpmsauron/bytepitchtest.git
    ```

2. At the project root directory, there should be a docker-compose.yml file.
    Execute it to build all images and launch the respective containers:

    ```
    docker-compose up
    ```

    The database will be created on the database service, and the Kafka topics A and B on the Kafka service.

3. You need now to set the service .env variables for each of the PHP/webserver services "requester", "broker", "servicea" and "serviceb". I recommend having four terminal tabs open to ease the configuration.
    Enter each container with the following commands, one for each, with the respective container assigned names (should be those by default):

    ```
    docker exec -it bytepitchtest_requester_1 bash
    docker exec -it bytepitchtest_broker_1 bash
    docker exec -it bytepitchtest_servicea_1 bash
    docker exec -it bytepitchtest_serviceb_1 bash
    ```

4. Inside each one, you should fallback right away onto the default *workdir* served by Apache (/var/www/html).
    For each one, copy the .env.example file on the app root folder into a .env file:

    ```
    cp .env.example .env
    ```

5. Edit the .env files and for each one, fill in the missing fields with the correct values (only the IP address fields for each related service should be in need of filling, other values should be dummy-filled with actual default-docker assigned parameters, such as serviceports and the db settings). Check the command at the last section here (Notes) to check the IP addresses for the Containers to ease the filling in.

6. From inside both servicea and serviceb containers, on the app root directory, run the Kafka consumer scripts that track the messages for each topic and subsequent processing:

    ```
    php Consumer.php
    ```

7. From inside the requester container, execute the request script to run a request onto the broker service:

    ```
    php request.php
    ```

8. On servicea script output you should see a entry for each message that was sent to Topic A in Kafka:

    ```
    Hi,
    ```

9. On seviceb script output you should see an entry for each message that was sent to Topic B in Kafka, which results from Topic A being consumed first then changed by servicea, saved to the database and retrieved back, in assynchronous format, so, what is output in serviceb may sometimes have delay in comparison to what outputs on servicea, sometimes it may be almost sequential.

    ```
    Hi, Mirzet
    ```

10. As the output for the requester service, you should see the token generated first:

    ```
    013e79e8a9ee079d31072d375a579c74
    ```

    and then, depending on whether the requester service was able to pull a response from the broker service in under one second or not, a valid response is shown if yes:

    ```
    Hi, Mirzet Bye
    Broker responded after 1.7116379737854 seconds and on 26 tries.
    ```

    or, if not, a "No response Exception" is thrown and the following shows:

    ```
    No Response
    ```

    It has been noticed however that, under a second, it is rare to be able to get a valid response, most times a response seems to take between over a second and slightly under two seconds. This max value can be increased editing this file [here](https://bitbucket.org/rpmsauron/bytepitchtest/src/8c4cff1d61e7c36d24976367b9ce61ee422b6ea6/requester/src/ResponseHandler.php#lines-10).

#### Useful commands / notes:
Manually create a Kafka topic from inside the Kafka container:
```
$KAFKA_HOME/bin/kafka-topics.sh --create --topic some_topic --partitions 4 --replication-factor 1 --bootstrap-server `broker-list.sh`
```
Check all Container IP addresses:
```
docker inspect --format='{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)
```
The PHP/webserver containers have telnet to ease any debugging on service connection.

A single point controller for the Broker service was implemented throught Apache configuration using Rewrite engine rules and the URI then parsed manually for each service endpoint, according to the method suggested in [here](https://stackoverflow.com/questions/39712599/redirect-all-requests-to-single-page)

I advise using a Postgres client such as TablePlus for connecting to the dbpostgres container database and verify the data, using the postgres container-assigned default credentials:

```
Host/Socket: localhost
User: postgres
Password: mysecretpassword
Database: broker
Port: 5432
```

Access postgres client inside container:

```
psql -U postgres
```

where <postgres> is the user.

Useful postgres queries:

```
\l  -shows databases
\c <database> - uses database <database>
\dt - shows tables for database in use
```

Rui M. Silva, 2021
