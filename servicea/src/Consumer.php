<?php

require "bootstrap.php";

use Kafka\ConsumerConfig;
use Kafka\Consumer;

$namesAppendable = [
    "Joao", 
    "Bram", 
    "Gabriel", 
    "Fehim", 
    "Eni", 
    "Patrick", 
    "Micha", 
    "Mirzet", 
    "Liliana", 
    "Sebastien"
];

$kafkaHost = $_ENV['KAFKA_HOST'];
$kafkaPort = $_ENV['KAFKA_PORT'];

$config    = ConsumerConfig::getInstance();
$config->setMetadataBrokerList($kafkaHost . ":" . $kafkaPort);
$config->setGroupId('A');
$config->setBrokerVersion('1.0.0');
$config->setTopics(['A']);
$consumer  = new Consumer();

$consumer->start(function($topic, $part, $message) use ($namesAppendable) {
    // Append a random name to message.
    $randomName     = $namesAppendable[rand(0, count($namesAppendable)-1)];
    $rawMessage     = $message["message"]["value"];
    echo "$rawMessage\n";
    $alteredMessage = $rawMessage . $randomName;

    $token          = $message["message"]["key"];
    $brokerHost     = $_ENV['BROKER_HOST'];

    // Send message back, but altered, to Broker service.
    $payload = json_encode(["message" => $alteredMessage, "token" => $token]);
    $handle  = curl_init();
    curl_setopt($handle, CURLOPT_URL, "http://" . $brokerHost . "/api/altered");
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_POST, 1);
    curl_setopt($handle, CURLOPT_POSTFIELDS, $payload);
    curl_exec($handle);
    curl_close($handle);
});