<?php

require "bootstrap.php";

use Src\Controller\RequestController;
use Kafka\Producer;
use Kafka\ProducerConfig;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, "
        . "Authorization, X-Requested-With");

$kafkaHost = $_ENV['KAFKA_HOST'];
$kafkaPort = $_ENV['KAFKA_PORT'];
$uri       = $_SERVER['REQUEST_URI'];

// Endpoint definition.
if ($uri == '/api/request') {
    $requestMethod = $_SERVER["REQUEST_METHOD"];
    $controller    = new RequestController($dbConnection, $requestMethod);
    $input         = json_decode(file_get_contents("php://input"), true);
    $response      = $controller->processRequestEndpoint($input);
    $token         = $response["body"];
    $msg           = "Hi, ";

    //Kafka: produce onto Topic A.
    $config = ProducerConfig::getInstance();
    $config->setMetadataRefreshIntervalMs(10000);
    $config->setMetadataBrokerList($kafkaHost . ":" . $kafkaPort);
    $config->setBrokerVersion('1.0.0');
    $config->setRequiredAck(1);
    $config->setIsAsyn(false);
    $config->setProduceInterval(500);

    $producer = new Producer(function() use ($msg, $token) {
        return [[
            'topic' => 'A',
            'value' => $msg,
            'key'   => $token
        ]];
    });

    $producer->success(function($result) {
    });

    $producer->error(function($errorCode) {
        echo $errorCode;
    });

    $producer->send(true);
} 

// Endpoint definition.
elseif ($uri == '/api/altered') {
    $requestMethod = $_SERVER["REQUEST_METHOD"];
    $controller    = new RequestController($dbConnection, $requestMethod);
    $input         = json_decode(file_get_contents("php://input"), true) ?? [];
    $controller->processAlteredEndpoint($input);
}

// Endpoint definition.
elseif (preg_match("#/api/response/[a-z0-9]+$#i", $uri, $matches)) {
    $requestMethod = $_SERVER["REQUEST_METHOD"];
    $controller    = new RequestController($dbConnection, $requestMethod);
    $parts         = explode("/", $uri);
    $token         = end($parts);
    $controller->processGetRequestEndpoint($token);
}

// Endpoint definition fallback.
else {
    header("HTTP/1.1 404 Not Found");
    exit("ERROR 404 NOT FOUND\n");
}

