<?php

namespace Src\Controller;

use Src\Repositories\RequestRepository;
use Kafka\ProducerConfig;
use Kafka\Producer;

class RequestController 
{
    private $requestMethod;
    private $requestRepository;

    public function __construct($db, $requestMethod)
    {
        $this->requestMethod     = $requestMethod;
        $this->requestRepository = new RequestRepository($db);
    }

    /**
     * Enpoint processing for route /api/request.
     *
     * @param  array $input The input parameters for the request entry to create.
     *
     * @return array The HTTP Response array.
     */
    public function processRequestEndpoint(array $input = [])
    {
        if ($this->requestMethod == "POST") {
            if(empty($input)) {
                $response = $this->emptyBodyResponse();
            } else {
                $response = $this->createRequest($input);
            }
        } else {
            $response = $this->notFoundResponse();
        }

        header($response['status_code_header']);
        
        if ($response['body']) {
            // HTTP Response body is the token generated.
            echo json_encode($response['body']);
        }

        return $response;
    }

    /**
     * Enpoint processing for route /api/altered.
     *
     * @param  array $input The input parameters for the request entry to alter.
     *
     * @return array The HTTP Response array.
     */
    public function processAlteredEndpoint(array $input = [])
    {
        if ($this->requestMethod == "POST") {
            $response['status_code_header'] = 'HTTP/1.1 200 OK';
            $response["body"]               = null;
            $this->produceToKafka("B", $input["message"], $input["token"]);
        } else {
            $response = $this->notFoundResponse();
        }

        header($response['status_code_header']);

        if ($response['body']) {
            echo json_encode($response['body']);
        }

        return $response;
    }

    /**
     * Enpoint processing for route /api/response/{token}.
     *
     * @param  string $token The input token parameter for the request entry to get.
     *
     * @return array The HTTP Response array.
     */
    public function processGetRequestEndpoint(string $token)
    {
        if ($this->requestMethod == "GET") {
            $response = $this->getRequest($token);
        } else {
            $response = $this->notFoundResponse();
        }

        header($response['status_code_header']);

        if ($response['body']) {
            // HTTP Response body is the full_message value retrieved.
            echo json_encode($response['body']);
        }

        return $response;
    }

    /**
     * Returns the HTTP Response from creating a request entry.
     *
     * @param  array $input The input parameters to create the request with.
     *
     * @return array The HTTP Response array.
     */
    private function createRequest(array $input)
    {
        $token                          = $this->requestRepository->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body']               = $token;

        return $response;
    }

    /**
     * Returns the HTTP Response for a not found resource.
     *
     * @return array The HTTP Response array.
     */
    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body']               = null;

        return $response;
    }

    /**
     * Returns the HTTP Response for an empty body POST Request.
     *
     * @return array The HTTP Response array.
     */
    private function emptyBodyResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 400 Bad Request';
        $response['body']               = null;

        return $response;
    }

    /**
     * Produces message to given Topic onto Kafka Server.
     *
     * @param string $topicName The topic in Kafka server to produce message onto.
     * @param string $message   The message to be produced.
     * @param string $token     The token identifying the message as Kafka's entry key.
     */
    private function produceToKafka(string $topicName, string $message, string $token)
    {
        $kafkaHost = $_ENV['KAFKA_HOST'];
        $kafkaPort = $_ENV['KAFKA_PORT'];

        $config    = ProducerConfig::getInstance();
        $config->setMetadataRefreshIntervalMs(10000);
        $config->setMetadataBrokerList($kafkaHost . ":" . $kafkaPort);
        $config->setBrokerVersion('1.0.0');
        $config->setRequiredAck(1);
        $config->setIsAsyn(false);
        $config->setProduceInterval(500);

        $producer  = new Producer(function() use ($message, $topicName, $token) {
            return [[
                'topic' => $topicName,
                'value' => $message,
                'key'   => $token
            ]];
        });

        $producer->success(function($result) {
            echo json_encode($result) ;
        });

        $producer->error(function($errorCode) {
            echo $errorCode;
        });

        $producer->send(true);
    }

    /**
     * Returns the HTTP Response from retrieving a request given on a token.
     *
     * @param  string $token The token input parameter to get the request entry.
     *
     * @return array The HTTP Response array.
     */
    private function getRequest(string $token)
    {
        $request                        = $this->requestRepository->find($token);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body']               = $request;

        return $response;
    }
}