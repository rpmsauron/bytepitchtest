<?php

namespace Src\Repositories;

class RequestRepository
{
    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Inserts a request entry on the database, generating an identifying 
     * token for it, and returns the token.
     *
     * @param  array $input The input array for the query.
     *
     * @return string The token column value of the inserted entry.
     */
    public function insert(array $input = []): string
    {
        $query = "INSERT INTO requests (token, body) VALUES (:token, :body) RETURNING id;";
        $token = bin2hex(random_bytes(16));

        try {
            $statement = $this->db->prepare($query);
            $statement->execute([
                'token' => $token,
                'body'  => $input["message"]
            ]);

            return $token;
        } catch (\PDOException $e) {
            echo("DB query Error\n");
            exit($e->getMessage());
        }    
    }

    /**
     * Returns the first entry from the database for the given token, based on the 
     * premise that it should be unique.
     * However, uniqueness of the value generated for it is not garanteed by the 
     * functions used.
     *
     * @param  string $token The token for which to find the entry.
     *
     * @return array The first entry with the given token.
     */
    public function find(string $token): array
    {
        $query = "SELECT * FROM requests WHERE token = :token;";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute([
                'token' => $token,
            ]);

            $result = $statement->fetch();

            return  $result;
        } catch (\PDOException $e) {
            echo("DB query Error\n");
            exit($e->getMessage());
        }    
    }
}