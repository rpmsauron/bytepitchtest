<?php

namespace Src\System;

class DatabaseConnector
{
    private $dbConnection = null;

    public function __construct()
    {
        $host     = $_ENV['DB_HOST'];
        $port     = $_ENV['DB_PORT'];
        $db       = $_ENV['DB_DATABASE'];
        $user     = $_ENV['DB_USERNAME'];
        $password = $_ENV['DB_PASSWORD'];

        try {
            $this->dbConnection = new \PDO(
                "pgsql:host=$host port=$port dbname=$db user=$user password=$password"
            );
        } catch (\PDOException $e) {
            echo("Database Conection Error");
            exit($e->getMessage());
        }

    }

    /**
     * Gets the PDO Database Connection.
     *
     * @return |PDO The PDO Connection.
     */
    public function getConnection()
    {
        return $this->dbConnection;
    }
}