CREATE DATABASE broker;

\c broker;

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS requests_id_seq;

-- Table Definition
CREATE TABLE IF NOT EXISTS requests (
    "token" varchar,
    "body" varchar,
    "full_message" varchar,
    "id" int4 NOT NULL DEFAULT nextval('requests_id_seq'::regclass),
    PRIMARY KEY ("id")
);


