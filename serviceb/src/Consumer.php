<?php

require "bootstrap.php";

use Kafka\ConsumerConfig;
use Kafka\Consumer;
use Src\Repositories\RequestRepository;

$config   = ConsumerConfig::getInstance();
$config->setMetadataBrokerList("192.168.1.138:9092");
$config->setGroupId('B');
$config->setBrokerVersion('1.0.0');
$config->setTopics(['B']);
$consumer = new Consumer();

$consumer->start(function($topic, $part, $message) use ($dbConnection) {
    $rawMessage   = $message["message"]["value"];
    echo("$rawMessage\n");
    $token        = $message["message"]["key"];
    $finalMessage = $rawMessage . " Bye";
    $repository   = new RequestRepository($dbConnection);
    $repository->update(["token" => $token], ["message" => $finalMessage]);
});