<?php

namespace Src\Repositories;

class RequestRepository
{
    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Updates a request entry on the database. 
     * token for it.
     *
     * @param  array $params The parameters array for the query (avalues for where conditions).
     * @param  array $input  The input array for the query (update values).
     */
    public function update(array $params = [], array $input = [])
    {
        $query = "UPDATE requests SET full_message = :message WHERE token = :token;";

        try {
            $statement = $this->db->prepare($query);
            $statement->execute([
                'message' => $input["message"],
                'token'   => $params["token"]
            ]);
        } catch (\PDOException $e) {
            echo("ERROR\n");
            exit($e->getMessage());
        }    
    }
}